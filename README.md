### How to run
This project uses the [SBT](http://scala-sbt.org/) as a build system.
You can run the tests by executing `sbt test`.

### Importing into IDEA
IDEA supports SBT by default when the Scala plugin is installed. To import this
project, go to File > New > Project from Existing Sources and choose SBT when
asked to `Import project from external model`