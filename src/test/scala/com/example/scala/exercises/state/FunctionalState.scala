package com.example.scala.exercises.state

import org.scalatest.{FreeSpec, Matchers}

class FunctionalStateTest extends FreeSpec with Matchers {

  "bla" - {
    val list = List.fill(1000)(FunctionalState.nonNegativeLessThanFMap(100))
//    List[Rand[Int]]
    val state = State.sequence(list)
//    State[List[Int]]
    val ints = state.apply(Generator(234L))
    ints._1.forall(i=> i >=0 && i < 100) shouldBe true
  }
}
