package com.example.scala.exercises.stream

import org.scalatest.{FunSpec, Matchers}

class StreamTest extends FunSpec with Matchers {

  describe("A stream") {

    describe("When converted to a List") {
      describe("Should return an empty list if the stream is Empty") {
        Stream.empty[Int].toList shouldBe List()
      }

      describe("Should return a list containing the evaluated elements if the Stream is not empty") {
        Stream(1, 2, 3, 4, 5).toList shouldBe List(1, 2, 3, 4, 5)
      }
    }

    describe("When taking elements from it") {
      describe("Should return an empty list if") {
        describe("the stream is empty") {
          Stream.empty[Int].take(5).toList shouldBe List()
        }

        describe("the number of elements to take is 0") {
          Stream(1, 2).take(0).toList shouldBe List()
        }

        describe("the number of elements to take is negative") {
          Stream(1, 2).take(-4).toList shouldBe List()
        }
      }

      describe("Should return a list containing the evaluated elements") {
        Stream(1, 2, 3, 4, 5).take(4).toList shouldBe List(1, 2, 3, 4)
      }

      describe("Should return a list containing all elements in the stream if number to take is greater stream size") {
        Stream(1, 2, 3).take(5).toList shouldBe List(1, 2, 3)
      }
    }

    describe("When dropping elements") {
      describe("Should return an empty stream if") {
        describe("the stream is empty") {
          Stream.empty[Int].drop(5).toList shouldBe Stream.empty[Int].toList
        }

        describe("the number of elements to drop is equal to the number of elements in the stream") {
          Stream(1, 2, 3).drop(3).toList shouldBe Stream.empty[Int].toList
        }

        describe("the number of elements to drop is greater than the number of elements in the stream") {
          Stream(1, 2, 3).drop(5).toList shouldBe Stream().toList
        }
      }

      describe("should return the same stream if") {
        describe("the number of elements to drop is 0") {
          Stream(1, 2, 3).drop(0).toList shouldBe Stream(1, 2, 3).toList
        }

        describe("the number of elements to drop is negative") {
          Stream(1, 2, 3).drop(-5).toList shouldBe Stream(1, 2, 3).toList
        }
      }

      describe("Should return a stream without the first n elements") {
        Stream(1, 2, 3, 4).drop(2).toList shouldBe Stream(3, 4).toList
      }
    }

    describe("Take while") {
      describe("Should return an empty stream if ") {
        describe("the stream is empty") {
          Stream.empty[Int].takeWhile(_ > 0).toList shouldBe Stream.empty[Int].toList
          Stream.empty[Int].takeWhileFoldRight(_ > 0).toList shouldBe Stream.empty[Int].toList
        }

        describe("none of the elements match the condition") {
          Stream(1, 2, 3, 4, 5).takeWhile(_ < 0).toList shouldBe Stream.empty[Int].toList
          Stream(1, 2, 3, 4, 5).takeWhileFoldRight(_ < 0).toList shouldBe Stream.empty[Int].toList
        }
      }

      describe("Should return all elements up until the first that doesn't match the condition") {
        Stream(1, 2, 3, 4, -5, 6, 7, -8, 9).takeWhile(_ > 0).toList shouldBe Stream(1, 2, 3, 4).toList
        Stream(1, 2, 3, 4, -5, 6, 7, -8, 9).takeWhileFoldRight(_ > 0).toList shouldBe Stream(1, 2, 3, 4).toList
      }
    }

    describe("forAll") {
      describe("Should return false if at least one of the elements does not match the condition") {
        Stream(1, 2, -3, 4).forAll(_ > 0) shouldBe false
      }

      describe("Should return true if") {
        describe("The stream is empty") {
          Stream.empty[Int].forAll(_ > 0) shouldBe true
        }
        describe("all elements match the condition") {
          Stream(1, 2, 3, 4).forAll(_ > 0) shouldBe true
        }
      }
    }

    describe("headOption") {
      describe("Should return None if stream is empty") {
        Stream.empty[Int].headOption shouldBe None
      }

      describe("Should return an option containing the first element if the stream is not empty") {
//        Stream.cons({println("bla"), 1}, Stream.cons({println("bla2", 2);2}, Stream.empty)).headOption shouldBe Option(1)
        Stream(1,2,3,4).headOption shouldBe Option(1)
      }
    }

    describe("map") {
      describe("Should return stream if is empty") {
        Stream.empty[Int].map(_+1).toList shouldBe Stream.empty[Int].toList
      }

      describe("Should return a stream that applies the provided function to the elements of this stream") {
//        Stream.cons({println("bla"); 1}, Stream.cons({println("bla2"); 2}, Stream.empty)).map( a => {println("incrementing");a+1}).toList
        Stream(1,2,3,4).map(_+1).toList shouldBe List(2,3,4,5)
      }
    }

    describe("filter") {
      describe("Should return an empty stream if is empty") {
        Stream.empty[Int].filter(_>0).toList shouldBe List()
      }

      describe("Should bla") {
//        Stream.cons({println("bla"); 1}, Stream.cons({println("bla2"); 2}, Stream.empty)).filter(_>0)
        Stream(1,-1,2,-2).filter(_>0).toList shouldBe List(1,2)
      }
    }

    describe("append") {
      Stream.empty.+(1).toList shouldBe List(1)
      Stream(1,2,3).+(4).toList shouldBe List(1,2,3,4)
      Stream(1,2,3).++(Stream(4,5,6)).toList shouldBe List(1,2,3,4,5,6)
    }

    describe("flatMap") {
//      Stream.cons({println("1");1}, Stream.cons({println("2");2}, Stream.empty)).flatMap(i=> {println(s"processing $i"); Stream(i,i)})
      Stream.empty[Int].flatMap(i=> Stream(i,i)).toList shouldBe List()
      Stream(1,2).flatMap(i=>Stream(i,i)).toList shouldBe List(1,1,2,2)
    }

    Stream.constant(42).take(3).toList shouldBe List(42,42,42)
    Stream.fibs.take(6).toList shouldBe List(0,1,1,2,3,5)
    Stream.from(5).take(5).toList shouldBe List(5,6,7,8,9)
  }
}