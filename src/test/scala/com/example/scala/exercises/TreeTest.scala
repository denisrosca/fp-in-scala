package com.example.scala.exercises

import org.scalatest.{FunSpec, Matchers}

class TreeTest extends FunSpec with Matchers {

  describe("A tree") {
    describe("Exercise 3.25 - Node count") {
      it("should return 1 if the tree is a leaf") {
        Leaf(1).size shouldBe 1
      }

      it("should return the number of nodes") {
        Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)).size shouldBe  5
      }
    }

    describe("Exercise 3.26 - Ordering") {
      it("should return the value if tree is a leaf") {
        Leaf(5).max shouldBe 5
      }

      it("should return the maximum of the leaf values") {
        Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)).max shouldBe 3
      }
    }

    describe("Exercise 3.27 - Depth") {
      it("should return 1 if it is a list") {
        Leaf(5).depth shouldBe 1
      }

      it("should return the number of levels it is a branch") {
        Branch(Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)), Leaf(4)).depth shouldBe 4
      }
    }

    describe("Exercise 3.28 - Map") {
      it("transforming a Leaf should return a Leaf") {
        Leaf(5) map(_+1) shouldBe Leaf(6)
      }

      it("transforming a branch should return a new branch with all leafs modified") {
        Branch(Branch(Leaf(1), Leaf(2)), Leaf(3)) map(_+1) shouldBe Branch(Branch(Leaf(2), Leaf(3)), Leaf(4))
      }

      it("allows to change the type during a map operation") {
        Branch(Leaf(1), Leaf(2)) map (_.toString) shouldBe Branch(Leaf("1"), Leaf("2"))
      }
    }
  }
}
