package com.example.scala.exercises

import org.scalatest.{FunSpec, Matchers}

class OptionTest extends FunSpec with Matchers {

  describe("Option") {
    describe("Map - transforming an option") {
      it("should return a transform value if the option was present") {
        Option(1).map(_+1) shouldBe Option(2)
      }

      it("should remain unchanged if the option was not present") {
        Option[Int]().map(_+1) shouldBe None
      }
    }

    describe("Flat map") {
      describe("if the option was not present") {
        it("should remain unchanged") {
          Option().flatMap(Option(_)) shouldBe Option()
        }
      }

      describe("if the option was present") {
        describe("but the transformation operation return a None") {
          it("should return a None") {
            Option(1).flatMap(v => None) shouldBe None
          }
        }

        it("should return an option with the transformed value") {
          Option(1).flatMap(v=>Option(v+1)) shouldBe Option(2)
        }
      }
    }

    describe("getOrElse") {
      it("should return the value if is present") {
        Option(1).getOrElse(2) shouldBe 1
      }

      it("should return the default value if option is not present") {
        Option().getOrElse(2) shouldBe 2
      }
    }

    describe("orElse") {
      it("should return same option if value is present") {
        Option(1).orElse(Option(2)) shouldBe Option(1)
      }

      it("should return default if value is not present") {
        Option().orElse(Option(2)) shouldBe Option(2)
      }
    }

    describe("filter") {
      it("should return None if option is not present") {
        Option[Int]().filter(_>0) shouldBe Option()
      }

      it("should return None if option is present but value does not satisfy condition") {
        Option(-1).filter(_>0) shouldBe Option()
      }

      it("should return same option if is present and value satisfies condition") {
        Option(1).filter(_>0) shouldBe Option(1)
      }
    }
  }
}
