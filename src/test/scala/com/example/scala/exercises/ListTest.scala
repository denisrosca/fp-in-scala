package com.example.scala.exercises

import org.scalatest.{FunSpec, Matchers}

class ListTest extends FunSpec with Matchers {

  describe("Set head on a Nil") {
    List().setHead(5) shouldBe Cons(5, Nil)
  }

  describe("Set head on a cons") {
    List(2).setHead(1) shouldBe Cons(1, Nil)
  }

  describe("Drop elements from an empty list") {
    List().drop(5) shouldBe Nil
  }

  describe("Drop 1 element from a single element list") {
    List(1).drop(1) shouldBe Nil
  }

  describe("Drop more elements than present in a list") {
    List(1).drop(2) shouldBe Nil
  }

  describe("Drop less elements than present in a list") {
    List(1,2,3).drop(2) shouldBe Cons(3, Nil)
  }

  describe("Drop while from empty list") {
    List().dropWhile((v)=>true) shouldBe Nil
  }

  describe("Drop from list only prefix matches") {
    List(1,2,3).dropWhile( _ < 3) shouldBe Cons(3, Nil)
  }

  describe("Drop from list nothing matches") {
    val list = List(1,2,3)
    list.dropWhile(_>3) shouldBe list
  }

  describe("Drop from list both prefix and suffix matches") {
    List(1,2,3,2,1).dropWhile(_ < 3) shouldBe Cons(3, Cons(2, Cons(1, Nil)))
  }

  describe("Test sum with foldRight") {
    List(1,2,3).foldRight(0)(_+_) shouldBe 6
  }

  describe("Exercise 3.6 init") {
    describe("On full list") {
      List(1,2,3,4).init shouldBe List(1,2,3)
    }

    describe("On Nil") {
      List().init shouldBe Nil
    }
  }

  describe("Exercise 3.8 Test concat with foldRight") {
    val init: List[Int] = Nil:List[Int]
    List(1,2,3).foldRight(init)(Cons(_,_)) shouldBe List(1,2,3) //3.8
  }

  describe("Exercise 3.9") {
    describe("Length of list") {
      List(1, 2, 3).length shouldBe 3
    }

    describe("Length of empty list should be 0") {
      List().length shouldBe 0
    }
  }

  describe("Exercise 3.10") {
    describe("foldLeft on empty list returns default value") {
      List[Int]().foldLeft(0)(_+_) shouldBe 0
    }

    describe("foldLeft on non-empty list") {
      List(1,2,3).foldLeft(1)(_*_) shouldBe 6
    }
  }

  describe("Exercise 3.12") {
    describe("Reverse of empty list is empty list") {
      List().reverse shouldBe List()
    }

    describe("Reverse of singleton list should be itself") {
      List(1).reverse shouldBe List(1)
    }

    describe("Reverse list") {
      List(1,2,3).reverse shouldBe List(3,2,1)
    }
  }

  describe("Exercise 3.14") {
    describe("Append to empty list yields a singleton list") {
      List[Int]().append(1) shouldBe List(1)
    }

    describe("Append to list") {
      List(1,2).append(3) shouldBe List(1,2,3)
    }
  }

  describe("Exercise 3.15 - flatten") {
    describe("Concat two empty lists yields an empty list") {
      List().concat(List()) shouldBe List()
    }

    describe("Concat of an empty list with a non-empty list yields non-empty list"){
      List().concat(List(1,2,3)) shouldBe List(1,2,3)
    }

    describe("Concat of two non-empty lists will yield a list containing the elements of first list then the second") {
      List(1,2,3) concat List(4,5,6) shouldBe List(1,2,3,4,5,6)
    }

    describe("Flatten an empty list") {
      List().flatten shouldBe List()
    }

    describe("Flatten a list of lists") {
      List(List(1,2), List(3,4)).flatten shouldBe List(1,2,3,4)
    }

    describe("Flatten a list of lists2") {
      List(List(1,2), List(3,4)).flattenRev shouldBe List(1,2,3,4)
    }
  }

  describe("Exercise 3.16 - 3.18 - map") {

    describe("Transforming an empty list yields an emtpy list") {
      List[Int]().map(_+1) shouldBe List()
    }

    describe("Transform a list by incrementing its elements") {
      List(1,2,3).map(_+1) shouldBe List(2,3,4)
    }

    describe("Transform a list by applying .toString on its elements") {
      List(1,2,3).map(_.toString) shouldBe List("1", "2", "3")
    }
  }

  describe("Exercise 3.19 - filter") {
    describe("Filtering an empty list yields an empty list") {
      List[Int]().filter(_ % 2 != 0) shouldBe List()
      List[Int]().filter2(_ % 2 != 0) shouldBe List()
    }

    describe("Filtering a non-empty list should return a new list containing only non-matching elements") {
      List(1,2,3,4).filter(_%2 !=0) shouldBe List(1,3)
      List(1,2,3,4).filter2(_%2 !=0) shouldBe List(1,3)
    }
  }

  describe("zip") {
    List("a", "b", "c").zipWith(List(1, 2, 3))(_ + "_" + _.toString) shouldBe List("a_1", "b_2", "c_3")
  }

  describe("sub sequence") {
    List(1,2,3,4,5).hasSubSequence(List(1,2)) shouldBe true
    List(1,2,3,4,5).hasSubSequence(List(1,2,4)) shouldBe false
//    List(1,2,3,4,5).hasSubSequence(List(4)) shouldBe true
    List(1,2,3,4,5).hasSubSequence(List()) shouldBe true
    List(1,2,3,4,5).hasSubSequence(List(2,3,5)) shouldBe false
    List(1,2,3,4,5).hasSubSequence(List(5)) shouldBe false
    List(1,2,3,4,5).hasSubSequence(List(2,2,3,4)) shouldBe false
    List(1,2,3,4,5).hasSubSequence(List(2,2,3,4)) shouldBe false
//    List(1,2,2,3,4,5).hasSubSequence(List(2,2,3,4)) shouldBe true
    List(1,2,5).hasSubSequence(List(1,2,5,6)) shouldBe false

  }

  describe("Exercise 3.20 - flatMap") {
    describe("flatMap on an empty list yields an empty list") {
      List[Int]().flatMap( a=> List(a,a)) shouldBe List()
    }

    describe("flatMap on a non-empty list") {
      List(1,2).flatMap( i => List(i,i+1)) shouldBe List(1,2,2,3)
    }
  }
}
