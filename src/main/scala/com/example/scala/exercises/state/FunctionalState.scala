package com.example.scala.exercises.state

object FunctionalState {

  import State._

  val int: Rand[Int] = State(_.nextInt)

  val nonNegativeInt: Rand[Int] = State(rng => {
    val (number, nextRng) = rng.nextInt
    if (number < 0)
      (-(number + 1), nextRng)
    else
      (number, nextRng)
  })

  def nonNegativeLessThanFMap(n: Int): Rand[Int] =
    flatMap(nonNegativeInt) { i =>
      val mod = i % n
      if (i + (n - 1) - mod >= 0) State.unit(mod) else nonNegativeLessThan(n)
    }

  def nonNegativeLessThan(n: Int): Rand[Int] = flatMap(nonNegativeInt)(i => {
    val mod = i % n
    if (mod < n)
      empty(mod)
    else
      nonNegativeLessThan(n)
  })

  val double: Rand[Double] = State(rng => {
    val (n, nextRng) = nonNegativeInt(rng)
    (n / Int.MaxValue.toDouble, nextRng)
  })

  def map[A, B](rand: Rand[A])(f: A => B): Rand[B] = flatMap(rand)(a => empty(f(a)))

  def map2[A, B, C](rand1: Rand[A], rand2: Rand[B])(f: (A, B) => C): Rand[C] =
    flatMap(rand1)(a => map(rand2)(b => f(a, b)))

  def flatMap[A, B](f: Rand[A])(g: A => Rand[B]): Rand[B] = State(rng => {
    val (a, r) = f(rng)
    g(a)(r)
  })

  def both[A, B](rand1: Rand[A], rand2: Rand[B]): Rand[(A, B)] = {
    map2(rand1, rand2)((_, _))
  }

  def sequence[A](ops: List[Rand[A]]): Rand[List[A]] = {
    val left: Rand[List[A]] = ops.foldLeft(empty(List.empty[A]))(map2(_, _)((l, i) => i :: l))
    map(left)(_.reverse)
  }

  def empty[A](a: A): Rand[A] = State(rng => (a, rng))

  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (i, iRng) = rng.nextInt
    val (d, dRng) = double(iRng)
    ((i, d), dRng)
  }

  def doubleInt(rng: RNG): ((Double, Int), RNG) = {
    val ((i, d), r) = intDouble(rng)
    ((d, i), r)
  }

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (d1, r1) = double(rng)
    val (d2, r2) = double(r1)
    val (d3, r3) = double(r2)
    ((d1, d2, d3), r3)
  }

  def ints(n: Int)(rng: RNG): (List[Int], RNG) = {
    (0 to n).foldLeft((List[Int](), rng)) { (t, _) =>
      val (i, r) = rng.nextInt
      (t._1 :+ i, r)
    }
  }
}

case class State[S, +A](run: S => (A, S)) {

  def flatMap[B](f: A => State[S, B]): State[S, B] = State(s => {
    val (a, s1) = run(s)
    f(a).run(s1)
  })

  def map[B](f: A => B): State[S, B] =
    flatMap(a => State(s => (f(a), s)))

  def map2[B, C](other: State[S, B])(f: (A, B) => C): State[S, C] = for {
    a <- this
    b <- other
  } yield f(a, b)

  def apply(s: S): (A, S) = run(s)


  //  def map[B](f: A =>B): State[S,B] = flatMap(a=> unit(f(a)))
  //
  //  def map2[B,C](other:State[S,B])(f: (A,B)=>C):State[S,C] =
  //    for {
  //      a <- this
  //      b <- other
  //    } yield f(a,b)
  //  //    flatMap(a => other.map(b => f(a,b)))
}

object State {
  type Rand[A] = State[RNG, A]

  def sequence[S, A](states: List[State[S, A]]): State[S, List[A]] =
    states.foldLeft(unit[S, List[A]](List.empty[A]))((acc, state) => acc.map2(state)((l, a) => a :: l))

  def unit[S, A](a: A): State[S, A] = State(s => (a, s))
}

trait RNG {
  def nextInt: (Int, RNG)
}

case class Generator(seed: Long) extends RNG {

  def nextInt: (Int, RNG) = {
    val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL // `&` is bitwise AND. We use the current seed to generate a new seed.
    val nextRNG = Generator(newSeed) // The next state, which is an `RNG` instance created from the new seed.
    val n = (newSeed >>> 16).toInt // `>>>` is right binary shift with zero fill. The value `n` is our new pseudo-random integer.
    (n, nextRNG) // The return value is a tuple containing both a pseudo-random integer and the next `RNG` state.
  }
}