package com.example.scala.exercises

import scala.annotation.tailrec

sealed trait List[+A] {
  def head: A
  def tail: List[A]
  def setHead[B>:A](head: B): List[B]

  def init:List[A] = {
    def loop(l: List[A]): List[A] = l match {
      case Cons(_, Nil) => Nil
      case Nil => Nil
      case _ => Cons(l.head, loop(l.tail))
    }

    loop(this)
  }

  def length:Int = foldLeft(0){(x,y) => x+1}

  def reverse: List[A] = foldLeft(Nil:List[A]) { (acc, item) =>
      Cons(item, acc)
  }

  //List(List(1,2), List(3,4), List(5,6))
  def flatten[B](implicit ev: A<:<List[B]): List[B] = foldLeft(List[B]()){ (acc, item) =>
    acc.concat(item)
  }

  def flattenRev[B](implicit ev: A<:<List[B]): List[B] = foldRight(List[B]()){ (item, acc) =>
    item.foldRight(acc)((item2, acc2) => {
      Cons(item2, acc2)
    })
  }

  def foldLeft[B](init:B)(op:(B, A)=>B):B = {
    @tailrec def loop(list:List[A], init: B):B = list match {
      case Nil => init
      case Cons(h, t) => loop(t, op(init, h))
    }
    loop(this, init)
  }

  def foldRight[B](init:B)(op:(A,B)=>B):B = reverse.foldLeft(init)((res, item) => op(item, res))

  def map[B](op:A=>B):List[B] = foldRight(List[B]())( (a,b)=>Cons(op(a),b))

  def flatMap[B](op:A=>List[B]) = foldRight(List[B]())(op(_).concat(_))

  def filter(p: A => Boolean): List[A] = foldRight(List[A]())( (a,b)=>{
    if(p(a))
      Cons(a, b)
    else
      b
  })

  def filter2(p: A => Boolean): List[A] = flatMap { item =>
    if(p(item))
      List(item)
    else
      List()
  }

  def zipWith[B,C](other: List[B])(op: (A,B)=>C) = {

    @tailrec
    def loop(l1: List[A], l2: List[B], acc: List[C]): List[C] = l2 match {
      case Nil => acc.reverse
      case _ => loop(l1.tail, l2.tail, Cons(op(l1.head, l2.head), acc))
    }

    loop(this, other, List())
  }

  def hasSubSequence[B>:A](list:List[B]):Boolean = {
    @tailrec def loopInner(first:List[A], second:List[B]):Boolean = second match {
      case Nil => true
      case Cons(h, t) if first == Nil => false
      case Cons(h, t) if first.head == h => loopInner(first.tail, t)
      case _ => false
    }

    @tailrec def loopOuter(first: List[A]): Boolean = first match {
      case Nil => false
      case _ => loopInner(first, list) || loopOuter(first.tail)
    }

    loopInner(this, list)
  }

  def append[B>:A](item:B):List[B] = reverse.foldLeft(List(item)){ (t, h) =>
    Cons(h, t)
  }

  def concat[B>:A](list:List[B]):List[B] = reverse.foldLeft(list) { (t, h) =>
    Cons(h, t)
  }

  def drop(n: Int) = {
    @tailrec def loop(list: List[A], count: Int): List[A] = list match {
      case Cons(h, t) if count > 0 => loop(t, count -1)
      case _ => list
    }

    loop(this, n)
  }

  def dropWhile(p: A => Boolean): List[A] = {
    @tailrec def loop(list: List[A]): List[A] = list match {
      case Cons(h, t) if p(h) => loop(t)
      case _ => list
    }

    loop(this)
  }
}

case object Nil extends List[Nothing] {
  override def head: Nothing = throw new NoSuchElementException("head of empty list")
  override def tail: List[Nothing] = throw new NoSuchElementException("tail of empty list")

  override def setHead[B](head: B): List[B] = Cons(head, Nil)
}
case class Cons[+A](head: A, tail: List[A]) extends List[A] {
  override def setHead[B>:A](h:B): List[B] = Cons(h, tail)
}

object List {
  def apply[A](items: A*):List[A] = if(items.isEmpty) Nil else Cons(items.head, apply(items.tail:_*))
//  def init[A](list: List[A]): List[A] =
}
