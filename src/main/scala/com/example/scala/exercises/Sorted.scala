package com.example.scala.exercises

import scala.annotation.tailrec

object Sorted {

  def isSorted[A](a: Array[A], ordered: (A, A) => Boolean): Boolean = {
    @tailrec
    def loop(first: Int, second: Int): Boolean = {
      ordered(a(first), a(second)) match {
        case false => false
        case true if second == a.length -1 => true
        case true => loop(second, second + 1)
      }

    }
    if(a.length <= 1)
      true
    else
      loop(0, 1)
  }

  def main(args: Array[String]) {
    val ordering = (first: Int, second: Int) => first <= second
    println(isSorted(Array(1), ordering))
    println(isSorted(Array(1, 2), ordering))
    println(isSorted(Array(1, 2, 2, 3), ordering))
    println(isSorted(Array(1, 2, 3, 2), ordering))
  }
}
