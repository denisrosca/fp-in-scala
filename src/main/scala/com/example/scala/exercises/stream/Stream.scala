package com.example.scala.exercises.stream

import scala.annotation.tailrec

sealed trait Stream[+A] {

  import Stream._

  def isEmpty: Boolean

  def headOption:Option[A] = {
    foldRight(Option.empty[A])((a,b) => Option(a))
  }

  def map[B>:A](op: A => B): Stream[B] = {
    def f(a: => A, b: =>Stream[B]) = Cons(() => op(a), () => b)
    foldRight(Stream.empty[B])(f)
  }

  def flatMap[B>:A](op: A => Stream[B]): Stream[B] = {
    foldRight(Stream.empty[B])((item, acc) => op(item) ++ acc)
  }

  def filter(p: A => Boolean): Stream[A] = {
    //FIXME: this is not fully lazy
    foldRight(Stream.empty[A])((a,b) => if(p(a)) cons(a, b.filter(p)) else b.filter(p))
  }

  def +[B>:A](item: => B): Stream[B] = {
    foldRight(cons(item, empty))(cons(_,_))
  }

  def ++[B>:A](other: => Stream[B]): Stream[B] = {
    foldRight(other)(cons(_,_))
  }

  final def toList: List[A] = {
    @tailrec def loop(s: Stream[A], acc: List[A]): List[A] = s match {
      case Cons(h, t) => loop(t(), h()::acc)
      case Empty => acc
    }

    loop(this, List()).reverse
  }

  def take(n: Int): Stream[A] = this match {
    case Cons(h, t) if n > 0 => Cons(h, () => t().take(n - 1))
    case _ => Empty
  }

  def takeToList(n: Int): List[A] = {
    @tailrec def loop(s: Stream[A], acc: List[A], n: Int): List[A] = s match {
      case Cons(h, t) if n > 0 => loop(t(), h() :: acc, n - 1)
      case _ => acc
    }

    loop(this, List(), n).reverse
  }

  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Cons(h, t) if p(h()) => Cons(h, () => t().takeWhile(p))
    case _ => Empty
  }

  def takeWhileFoldRight(p: A => Boolean): Stream[A] = {
    def cond(item: => A, rest: => Stream[A]): Stream[A] = {
      if(p(item)) Cons( () => item, () => rest)
      else Empty
    }
    foldRight(Stream.empty[A])(cond)
  }

  @tailrec final def drop(n: Int): Stream[A] = this match {
    case Cons(h, t) if n > 0 => t().drop(n - 1)
    case _ => this
  }

  @tailrec final def forAll(p: A => Boolean): Boolean = this match {
    case Cons(h, t) => p(h()) && t().forAll(p)
    case _ => true
  }

  final def foldRight[B](init: =>B)(f: (=> A, => B) => B): B = this match {
    case Cons(h, t) => f(h(), t().foldRight(init)(f))
    case _ => init
  }

//  def zipWith[B,C](other: Stream[A])(op: (A,B)=>C): Stream[C] = ???
}

case object Empty extends Stream[Nothing] {
  override def isEmpty: Boolean = true
}

case class Cons[A](head: () => A, tail: () => Stream[A]) extends Stream[A] {
  override def isEmpty: Boolean = false
}

object Stream {
  def apply[A](items: A*): Stream[A] =
    if (items.isEmpty) Empty
    else cons(items.head, apply(items.tail: _*))

  def empty[A]: Stream[A] = Empty

  def cons[A](head: => A, tail: => Stream[A]): Stream[A] = {
    lazy val h = head
    lazy val t = tail
    Cons(() => h, () => t)
  }

  def unfold[A,S](z:S)(f: S=>Option[(A,S)]): Stream[A] = {
    f(z) match {
      case Some((a,s)) => Stream.cons(a, unfold(s)(f))
      case None => Stream.empty
    }
  }

  def constant[A](item: => A): Stream[A] = unfold(Option(item))(s=> s.map(i=> (i, Option(i))))
  def constantWithoutUnfold[A](item: => A): Stream[A] = cons(item, constant(item))

  def from(n: => Int): Stream[Int] = unfold(Option(n))(opt => opt.map(i => (i, Option(i+1))))
  def fromWithoutUnfold(n: => Int): Stream[Int] = cons(n, from(n+1))

  def fibs:Stream[Int] = unfold(Option((0,1)))(opt => opt.map(t => (t._1, Option(t._2, t._1 + t._2))))
  def fibsWithoutUnfold: Stream[Int] = {
    def loop(i: => Int, j: => Int): Stream[Int] = {
      cons(i, loop(j, i+j))
    }
    loop(0,1)
  }
}