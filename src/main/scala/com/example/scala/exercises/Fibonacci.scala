package com.example.scala.exercises

import scala.annotation.tailrec

object Fibonacci {

  def fibonacci(n: Int): Int = {
    @tailrec def go(n: Int, previous: Int, next: Int): Int = {
      n match {
        case 0 => previous
        case 1 => next
        case _ => go(n-1, next, next + previous)
      }
    }
    if(n < 0)
      throw new RuntimeException()
    go(n, 0, 1)
  }

  def main(args: Array[String]) {
    println(fibonacci(0))
    println(fibonacci(1))
    println(fibonacci(3))
    println(fibonacci(4))
    println(fibonacci(5))
  }
}