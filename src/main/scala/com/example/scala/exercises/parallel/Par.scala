package com.example.scala.exercises.parallel

import scala.concurrent.{ExecutionContext, Future}

object Par {
  type Par[A] = ExecutionContext => Future[A]

  def pure[A](op: A): Par[A] = context => Future.successful(op)

  def async[A,B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

  def lazyUnit[A](op: => A): Par[A] = fork(pure(op))

  def fork[A](op: => Par[A]): Par[A] = context => {
    implicit val implicitContext = context
    for {
      r <- op(context)
    } yield r
  }

  def map[A,B](p: Par[A])(f: A => B): Par[B] = p.flatMap(a=>pure(f(a)))

  def flatMap[A,B](p: Par[A])(f: A => Par[B]): Par[B] = context => {
    implicit val implicitContext = context
    for {
      a <- p(context)
      res <- f(a)(context)
    } yield res
  }

  def sequence[A](l: List[Par[A]]): Par[List[A]] = l.foldLeft(pure(List.empty[A])){
    (p1,p2) => p1.map2(p2)((acc, e) => e :: acc)
  }

  def map2[A, B, C](p1: Par[A], p2: Par[B])(f: (A, B) => C): Par[C] = for {
    r1 <- p1
    r2 <- p2
  } yield f(r1,r2)

  def parMap[A,B](ps: List[A])(f: A => B): Par[List[B]] = fork {
    sequence(ps.map(async(f)))
  }

  def parFilter[A](ps: List[A])(p: A => Boolean): Par[List[A]] =
    parMap(ps){ element =>
        if(p(element)) List(element) else List.empty
    }.map(_.flatten)

  implicit class ParOps[A](p: Par[A]) {
    def map[B](f: A=>B):Par[B] = Par.map(p)(f)
    def flatMap[B](f: A=>Par[B]): Par[B] = Par.flatMap(p)(f)
    def map2[B,C](other: Par[B])(f: (A,B) => C): Par[C] = Par.map2(p,other)(f)
  }
}

object Examples {
  import Par._

  def parSum(ints: IndexedSeq[Int]): Par[Int] = {
    if (ints.size <= 1)
      pure(ints.headOption getOrElse 0) // `headOption` is a method defined on all collections in Scala. We saw this function in chapter 3.
    else {
      val (l,r) = ints.splitAt(ints.length/2) // Divide the sequence in half using the `splitAt` function.
      parSum(l).map2(parSum(r))(_+_) // Recursively sum both halves and add the results together.
    }
  }
}