package com.example.scala.exercises

sealed trait Tree[+A] {

  def size: Int = fold(value=>1)(1 + _ + _)
  def max[B >: A](implicit ord: Ordering[B]): B = fold(_.asInstanceOf[B])(ord.max)
  def depth: Int = fold(value => 1)(1 + _.max(_))
  def map[B](op: A => B): Tree[B] = fold(value => Tree(op(value)))(Tree(_,_))

  def fold[B](op: A=>B)(f: (B, B)=>B): B = {
    def loop(tree:Tree[A]):B = tree match {
      case Leaf(value) => op(value)
      case Branch(lb, rb) => f(loop(lb), loop(rb))
    }
    loop(this)
  }
}

case class Leaf[A](value: A) extends Tree[A]

case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

object Tree {
  def apply[A](value: A):Tree[A] = Leaf(value)
  def apply[A,B>:A](lb: Tree[A], rb: Tree[A]): Tree[B] = Branch[B](lb, rb)
}