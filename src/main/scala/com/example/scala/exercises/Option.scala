package com.example.scala.exercises

sealed trait Option[+A] {

  def get: A

  def map[B](op: A=>B): Option[B] = fold(value => Option(op(value)))(None)

  def flatMap[B](op: A => Option[B]): Option[B] = fold(op)(None)

  def getOrElse[B>:A](default: => B): B = fold(v => v.asInstanceOf[B])(default)

  def orElse[B>:A](default: => Option[B]):Option[B] = fold(Option[B](_))(default)

  def filter(p: A => Boolean): Option[A] = flatMap { v =>
    if(p(v)) Option(v) else None
  }

  def fold[B](op: A => B)(onEmpty: => B): B = this match {
    case Some(value) => op(value)
    case _ => onEmpty
  }
}

case class Some[+A](value: A) extends Option[A] {
  override def get = value
}
case object None extends Option[Nothing] {
  override def get = throw new NoSuchElementException("Calling #get on None")
}

object Option {
  def apply[A](): Option[A] = None
  def apply[A](value: A): Option[A] = if(value == null) None else Some(value)
}