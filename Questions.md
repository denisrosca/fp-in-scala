## Chapter 3
#### Exercise 3.7
Can foldRight be implemented to short-circuit on certain condition? How?
How will it behave for large lists?

#### Exercise 3.8
What does this say about the relationship between foldRight and data
constructors?

#### Exercise 3.13
Can you implement foldLeft in terms of foldRight? How about the other way
around?

#### Exercise 3.14
Implement append in terms of either foldLeft or foldRight.

I did it with reverse and then fold left, but it seems stupid and consumes a
lot of memory. Can it be done any other way?

#### Exercise 3.15
Really cool type system feature with generalized type constraints example

## Chapter 4
### Exercise 4.2
Did someone implement it? How?

### Does anyone use interfaces in scala?
### Investigate flattenRev...

## Chapter 5
### Exercise 5.7
Did someone implement **filter** to be fully lazy